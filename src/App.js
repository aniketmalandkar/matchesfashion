import React from "react";
import Nav from "./components/Nav";
import WordFinder from "./components/WordFinder";

function App() {
  return (
    <div className="App">
      <header>
        <Nav></Nav>
      </header>

      <div className="body container">
        <WordFinder />
      </div>
    </div>
  );
}

export default App;
