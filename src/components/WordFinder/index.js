import React, { useRef, useEffect, useState } from "react";
import { findDuplicates } from "../../shared/utils";

const WordFinder = () => {
  const [data, setData] = useState("");
  const [showResult, setShowResult] = useState(false);
  const [duplicates, setDuplicates] = useState([]);
  const [allWords, setAllWords] = useState({});

  const $textArea = useRef(null);

  const onFindClick = () => {
    const { top3Duplicates, wordsWithCount } = findDuplicates(data);

    setDuplicates(top3Duplicates);
    setAllWords(wordsWithCount);
    setShowResult(true);
  };

  const onClearClick = () => {
    setData("");
    setShowResult(false);
    setDuplicates([]);
    setAllWords({});

    $textArea.current.focus();
  };

  useEffect(() => $textArea.current.focus(), []);

  const renderDuplicates = () => {
    if (duplicates.length > 0) {
      const top3DuplicateBody = duplicates.map((data, index) => {
        return (
          <tr key={index}>
            <td>{data.word}</td>
            <td>{data.cnt}</td>
          </tr>
        );
      });

      const allWordsBody = Object.keys(allWords).map((data, index) => {
        return (
          <tr key={index}>
            <td>{data}</td>
            <td>{allWords[data]}</td>
          </tr>
        );
      });

      return (
        <React.Fragment>
          {renderTable("Top 3 Duplicates", top3DuplicateBody)}
          {renderTable("All Words With Count", allWordsBody)}
        </React.Fragment>
      )
    } else {
      return "No Duplicates Found.";
    }
  };

  const renderTable = (heading, tableBody) => {
    return (
      <React.Fragment>
        <h5>{heading}</h5>

        <table className="table table-dark">
          <caption className="sr-only">{heading}</caption>

          <thead>
            <tr>
              <th>Word</th>
              <th>Number of Occurrences</th>
            </tr>
          </thead>

          <tbody>{tableBody}</tbody>
        </table>
      </React.Fragment>
    );
  }

  return (
    <div>
      <h1 className="my-5 text-center">Duplicate Word Finder</h1>

      <form>
        <div className="form-group">
          <label htmlFor="txt">Input String:</label>
          <textarea className="form-control" id="txt" ref={$textArea} value={data} onChange={(e) => setData(e.target.value)} />
          <small id="input-help" className="form-text text-muted">Please enter any string without character limitation.</small>
        </div>

        <button type="button" className="btn btn-primary" onClick={onFindClick}>Find Duplicates</button>
        <button type="button" className="btn btn-danger ml-3" onClick={onClearClick}>Clear</button>
      </form>

      {showResult ? <div className="mt-5">{renderDuplicates()}</div> : ""}
    </div>
  );
};

export default WordFinder;
