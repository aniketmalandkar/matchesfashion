/**
 * returns top 3 repeated word and all words with their count
 * @param {string} data 
 */
const findDuplicates = (data = "") => {
  const wordsWithCount = {};
  const top3Duplicates = [];

  const dataArray = data
    .replace(/\n/g, " ") // Replace new line with space
    .replace(/[^a-zA-Z' ]/g, "") // No need to handle fancy punctuation except apostrophe. That's why replace all with empty string
    .toLowerCase() // convert all data with lower case
    .split(" "); // split with space to convert to an array of words


  let i = 0;
  let j = dataArray.length - 1;

  while (i < j) {
    insertWordIntoDuplicates(dataArray[i], top3Duplicates, wordsWithCount);
    insertWordIntoDuplicates(dataArray[j], top3Duplicates, wordsWithCount);
    i++;
    j--;
  }

  if (i === j) {
    insertWordIntoDuplicates(dataArray[i], top3Duplicates, wordsWithCount);
  }

  return { top3Duplicates, wordsWithCount };
};

/**
 * Increase count of words of word present in wordsWithCount or else adds new.
 * Maintains top 3 repeated duplicates
 * @param {string} word 
 * @param {array} top3Duplicates 
 * @param {object} wordsWithCount 
 */
const insertWordIntoDuplicates = (word, top3Duplicates, wordsWithCount) => {
  if (!word) return;

  let cnt = wordsWithCount[word]; // Caching object data to variable (Looking into object every time is heavy operation)
  cnt = cnt ? ++cnt : 1;
  wordsWithCount[word] = cnt;

  const isPresent = top3Duplicates.find((currentObj) => (currentObj.word === word) ? currentObj.cnt = cnt : false);

  if (!isPresent) {
    top3Duplicates.push({ word, cnt });
  }

  top3Duplicates.sort((a, b) => b.cnt - a.cnt);

  if (top3Duplicates.length === 4) {
    top3Duplicates.pop();
  }
};

export { findDuplicates };
